# ign-plugin-release

The ign-plugin-release repository has moved to: https://github.com/ignition-release/ign-plugin-release

Until May 31st 2020, the mercurial repository can be found at: https://bitbucket.org/osrf-migrated/ign-plugin-release
